---
date: "2018-10-22T05:00:00.000Z"
title: "Hello World"
tags:
  - intro
  - about
featured: ui-and-code.png
excerpt: Who I am, what I do, and why you should care! This post goes into some detail about my general philosophy around blogging, what I hope to accomplish with this blog, and various other details…
---

<div>
<video class="responsive" autoplay="true" loop="true">
  <source type="video/mp4" src="https://media.giphy.com/media/QQkyLVLAbQRKU/giphy.mp4"></source>
  <p>Your browser does not support the video element.</p>
</video>
</div>

Hello! I am Sachin Maharana.I am a Software developer based out of bangalore. I have been working with dev ops team at my current company. I have been working with golang, kubernetes, docker, ansible for past few months. Before that i dabbled with node.js and react as part of platform team.

I have been planning to write my unfiltered thoughts for quite some time. Both for postereity as well as bringing a order to chaos that is my mind by putting myself out there.
. You can get to this site by https://blog.sachinmaharana.in. I will be changing a lot of things coming down the line, owing to the time i get, but just wanted to pull together a working prototype and i think i am just able to acheive that.

Not sure how often i will post here, but hopefully frequently. Promises! Promises:)
I guess i will change things here and there as i move along and get comfortable with this medium.

You can contact me on sachin.nicky@gmail.com. Unfortunately i am not facebook or any other popular social media site. I am active on reddit and twitter(https://twitter.com/iSachinMaharana) only. I occasioanlly post on instagram (https://www.instagram.com/sachin.nicky/). Feel free to DM me in twitter. Don't hesitate.I promise i will be good.

xoxo.
