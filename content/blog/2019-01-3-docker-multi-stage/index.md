---
date: "2019-01-03T05:00:00.000Z"
title: "Golang With Docker"
tags:
 
featured: ui-and-code.png
excerpt: Developing Go App With Docker
---

So you have created a go app, now what? Well, why keep it to yourself and have all the fun. Why not distribute amongst your colleagues and peers.What do you really need is to create a docker image to distribute your application.

But docker image of you golang app can become too much in size if not kept in check, as i recently had experienced in my workplace. To solve this you have the option of using docker multi stage builds.

I have created a sample app, which could be found at https://github.com/SachinMaharana/go-app. It is a simple HTTP server running on port 8000 that will return the hostname of the server where it is deployed.You can also see the Dockerfile i have added there.

Multi-stage allows us to create Dockerfile with multiple **FROM**. It is very useful, as it enables us to build the app with all the required tooling, for example using the golang base image, and then having a second stage with just the binary built from the intermediate/first image. The last stage of a multi-stage build will be the image you will publish to your repository and deploy.

The Dockerfile with multi-stage builds in the example:

![alt text](full-1.png "Dockerfile")

In this example, the build stage will compile our application.
The last and final stage prod will be the image we will publish to our repository. It uses the binary from the build stage. You can also include any intermediate stage if you wish to.

## BUILD STAGE

The Build stage is responsible of building our Golang binary from the golang base image. This base image contains all the required tooling to compile our application to a binary executable.

![alt text](build.png "Build stage")

##### Line 1

The base image to use (golang:1.10) and we use the syntax as to give a name to this stage.

##### Line 2

We set our working directory to be the directory of our application in the default $GOPATH of the golang image

##### Line 3

We add the source files of our application.

##### Line 4

We build our binary. The different parameters are to create a totally static library, as our scratch image in prod will not contain the C libraries and everything the Golang VM may require.

We can now build this image with docker. Our application works as expected.

```bash
❯ docker build -t go-app .
Sending build context to Docker daemon  9.223MB
Step 1/8 : FROM golang:1.10 AS build
 ---> d0e7a411e3da
Step 2/8 : WORKDIR /go/src/github.com/sachinmaharana/go-app
 ---> Running in 433c8da77500
Removing intermediate container 433c8da77500
 ---> 6369afc1f287
Step 3/8 : ADD . .
 ---> c2a557f55e42
Step 4/8 : RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo .
 ---> Running in 5bda21f496e0
Removing intermediate container 5bda21f496e0
 ---> fbd028624269
Step 5/8 : CMD ["./go-app"]
 ---> Running in 85da1085fee1
Removing intermediate container 85da1085fee1
 ---> 5c0366dbb4b8
Step 6/8 : FROM scratch as prod
 --->
Step 7/8 : COPY --from=build /go/src/github.com/sachinmaharana/go-app .
 ---> 02238df3f011
Step 8/8 : CMD ["./go-app"]
 ---> Running in b3cda01794c2
Removing intermediate container b3cda01794c2
 ---> 36559f9a6201
Successfully built 36559f9a6201
Successfully tagged go-app:latest

❯ docker run --rm -ti -p 8000:8000 go-app
Server is running now

In another Terminal..

❯ curl localhost:8000
You have hit ea66eaddaa7b
```

let's have a look at the image size, using docker images.
![alt text](Selection_004.png "Build stage")

825MB on disk? That's a really bad deal.

Let's improve this situation, and reduce the size of our image considerably!Under 20MB maybe?

Let's implement a prod stage for our image. As explained above, this stage will just copy the binary from the build stage into the container.

![alt text](full-1.png "Build stage")

## PROD STAGE

As you can see, in the same Dockerfile we added a second FROM clause. This time, we use from scratch, as we don't have any dependencies.

##### Line 9

The base image being scratch

##### Line 10

We copy from the build stage the file located at /go/src/github.com/sachinmaharana/go-app

##### Line 11

We set the default command to start our application

Easy Peasy!

Let's build and run our image, just like before:

```bash
❯ docker build -t go-app .
❯ docker run --rm -ti -p 8000:8000 go-app
```

Let's have a look at image now using docker images

![alt text](selec2.png "Prod stage")

Let's than 20MB! And on the repository, it is only 2MB. When you'll start your container, you will download only 2MB. So much bandwidth and time saved compared to the previous version!

You are now able to create very lightweight image for your Golang application. The concept of stage build can be really useful for a lot of other use cases.

If you have any questions or found any discrepancies and others, shoot me a DM at https://twitter.com/iSachinMaharana or send me mail at sachin.nicky@gmail.com

Thanks!
