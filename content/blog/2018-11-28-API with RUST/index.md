---
date: '2018-11-28T23:36:56.503Z'
title: API with rust
tags:
  - API with rust
featured: ui-and-code.png
excerpt: Request With Rust.
---

I have been working with rust programming language for my personal projects and thought of blogging about it. I find working with rust very challenging and satisfying at the same time.I am still learning, and am at a explotary phase, so if you find a bug or improvement in the code, please send me a pull request.

The code for this could be found at:

https://github.com/SachinMaharana/rgit

Rust is a new programming language from Mozilla, the company that makes Firefox.

Rust is similar to C++ in performance and low-level ability but has a type system which automatically prevents whole classes of nasty bugs that are common in C++ programs.

This small utility i created is rgit. The problem i was trying to solve with this was that i was creating lot of github repo manually. And i found going to web interface(github.com) and creating new repo everytime i wanted to a bit tiring. So in alignment with the fact that how lazy i am, i created a cli command to automate it. So how do you use it?

Simple.

```bash
> rgit repoName repoDescription
```

And voila! My utilty will create a repo for you in your github profile. Cool right?

```rust
fn main() {
    dotenv().ok();
    let args: Vec<String> = env::args().collect();

    let config = Config::new(&args).unwrap_or_else(|err| {
        println!("Problem parsing arguments {}", err);
        process::exit(1);
    });

    let c = config.clone();

    match make_request(config) {
        Ok(resp) => {
            if resp.status().is_success() {
                println!("Success! Repo {} created.", c.repo_name);
                println!("Push an existing repository from the command line");
                println!(
                    "git remote add origin https://github.com/SachinMaharana/{}.git",
                    c.repo_name
                );
            } else if resp.status().is_server_error() {
                println!("Server error!");
            } else {
                println!("Something else happened. Github Says: {:?}", resp.status());
                println!("Repo {} Might Already Exist!", c.repo_name);
            }
        }
        Err(e) => {
            println!("Error during request.");
            println!("{}", e);
        }
    };
    // list_repo()
}
```

You can go through the code above and let me know. There is still lot to improve and will expand on it. Probably will write a tutorial on it. keep watching this space. Thanks
