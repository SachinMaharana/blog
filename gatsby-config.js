module.exports = {
  pathPrefix: '/',
  siteMetadata: {
    title: 'Sachin Maharana - Blog',
    author: 'Sachin Maharana',
  },
  plugins: [{
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-124814241-1",
        // Puts tracking script in the head instead of the body
        head: true,
        // Setting this parameter is optional
        anonymize: true,
        // Setting this parameter is also optional
        respectDNT: true,
        // Avoids sending pageview hits from custom paths
        exclude: ["/preview/**", "/do-not-track/me/too/"],
      },
    }, {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: `Sachin Maharana's Blog`,
        description: 'The blog of the developer, Sachin Maharna',
        short_name: 'Sachin Blog',
        background_color: 'white',
        theme_color: '#002635',
        orientation: 'portrait',
        display: 'minimal-ui',
        icon: `${__dirname}/assets/blog.png`
      }
    },
    'gatsby-plugin-catch-links',
    'gatsby-plugin-emotion',
    'gatsby-plugin-remove-trailing-slashes',
    'gatsby-plugin-twitter',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/content/blog`,
        name: 'post',
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          'gatsby-remark-copy-linked-files',
          {
            resolve: 'gatsby-remark-images',
            options: {
              backgroundColor: 'transparent',
              linkImagesToOriginal: false,
              showCaptions: true
            }
          },
          'gatsby-remark-prismjs',
          'gatsby-remark-smartypants',
          'gatsby-remark-autolink-headers'
        ]
      }
    },
    'gatsby-plugin-react-helmet',
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    {
      resolve: 'gatsby-plugin-typography',
      options: {
        omitGoogleFont: true,
        pathToConfigModule: 'src/utils/typography'
      }
    },
  ],
}