let versions = [{
    verison: "1.0.94",
    data: [{
        site: "32835",
        ts: "2018-08-08T22:39:51Z",
        stage: "DEV"
      },
      {
        site: "32836",
        ts: "2018-08-08T22:45:18Z",
        stage: "TEST1"
      },
      {
        site: "32899",
        ts: "2018-08-09T14:02:53Z",
        stage: "CERT1"
      }
    ]
  },
  {
    verison: "1.0.95",
    data: [{
        site: "32835",
        ts: "2018-08-08T22:39:51Z",
        stage: "DEV"
      },
      {
        site: "32898",
        ts: "2018-08-08T22:45:17Z",
        stage: "TEST1"
      },
      {
        site: "32898",
        ts: "2018-08-08T22:45:17Z",
        stage: "PROD"
      }
    ]
  }
];

let sVersions = versions.reduce((acc, obj) => {
  let key = obj['verison']
  if (!acc[key]) {
    acc[key] = [];
  }
  acc[key] = obj.data.map(s => s.stage)
  return acc
}, {})

// console.log(sVersions);
const entries = Object.entries(sVersions)
// console.log(entries);



let devData = () => {
  let x = []
  for (const [version, stages] of entries) {
    if (new Set(stages).has('DEV')) {
      x.push(20)
    } else {
      x.push(0)
    }
  }
  return x
}

let testData = () => {
  let x = []
  for (const [version, stages] of entries) {
    if (new Set(stages).has('TEST1')) {
      x.push(20)
    } else {
      x.push(0)
    }
  }
  return x
}


let certData = () => {
  let x = []
  for (const [version, stages] of entries) {
    if (new Set(stages).has('CERT1')) {
      x.push(20)
    } else {
      x.push(0)
    }
  }
  return x
}


let prodData = () => {
  let x = []
  for (const [version, stages] of entries) {
    if (new Set(stages).has('PROD')) {
      x.push(20)
    } else {
      x.push(0)
    }
  }
  return x
}



[
  ['1.0.94', ['DEV', 'TEST1', 'PROD']],
  ['1.0.95', ['DEV', 'TEST1']]
]

console.log(devData());
console.log(testData());
console.log(certData());
console.log(prodData());